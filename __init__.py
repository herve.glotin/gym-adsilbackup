from gym.envs.registration import register

register(
    id='adsil-v0',
    entry_point='gym_adsil.envs:adsilEnv',
)

#si on veut d'autres environnements comme un env extrahard.
#register(
    #id='ADSIL-extrahard-v0',
    #entry_point='gym_ADSIL.envs:ADSILExtraHardEnv',
#)